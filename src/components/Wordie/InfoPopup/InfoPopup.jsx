import styles from './InfoPopup.module.css'

export default function InfoPopup(props) {
  return (
    <div
      class={styles.modalContainer}
    >
      <div
        class={styles.modal}
      >
        {props.children}
      </div>
    </div>
  )
}